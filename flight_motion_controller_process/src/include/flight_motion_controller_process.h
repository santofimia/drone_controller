#ifndef flight_motion_controller_process
#define flight_motion_controller_process

/*!*************************************************************************************
 *  \class     flight_motion_controller_process
 *
 *  \brief     Flight Motion Controller Process
 *
 *  \details   This process is in charge of setting the controllers in order to use
 *             different modes.
 *
 *
 *  \authors   Pablo Santofimia Ruiz
 *
 *  \copyright Copyright 2016 Universidad Politecnica de Madrid (UPM)
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program. If not, see http://www.gnu.org/licenses/.
 ***************************************************************************************/

#include <string>

#include "ros/ros.h"
#include "droneModuleROS.h"
#include "communication_definition.h"
#include "yaw_controller.h"
#include "altitude_controller.h"
#include "position_controller.h"
#include "position_to_command_controller.h"
#include "speed_controller.h"
#include "speed_controllernn.h"
#include "trajectory_controller.h"
#include "trajectory_speed_controller.h"
//Drone msgs
#include "droneMsgsROS/dronePitchRollCmd.h"
#include "droneMsgsROS/droneDYawCmd.h"
#include "droneMsgsROS/droneDAltitudeCmd.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionTrajectoryRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/droneTrajectoryControllerControlMode.h"
#include "droneMsgsROS/droneAltitudeCmd.h"
#include "control/Controller_MidLevel_controlModes.h"
#include "control/simpletrajectorywaypoint.h"
//Services
#include "droneMsgsROS/setControlMode.h"

// CVG utils
#include "xmlfilereader.h"
#include "cvg_string_conversions.h"

class FlightMotionControllerProcess : public DroneModule
{
public:
    FlightMotionControllerProcess();
    ~FlightMotionControllerProcess();

    // DroneModule stuff: reset, start, stop, run, init, open, close
private:
    void init();
    void close();
protected:
    bool resetValues();
    bool startVal();
    bool stopVal();
public:
    bool run();
    void open(ros::NodeHandle & nIn);
protected:
    void readParameters();
public:
    bool readConfigs(std::string configFile);


private:
    AltitudeController altitude_control;
    YawController yaw_control;
    PositionController position_control;
    PostoCmdController poscmd_control;
    SpeedController speed_control;
//    SpeedControllerNN speed_control;
//    TrajectoryController trajectory_control;
    TrajectorySpeedController trajectory_control;


private:
    std::string config_file;

    // [Subscribers] Controller references
private:
    std::string dronePositionRefTopicName;
    ros::Subscriber dronePositionRefSub;
    void dronePositionRefsSubCallback(const droneMsgsROS::dronePositionRefCommandStamped::ConstPtr &msg);

    std::string droneSpeedsRefTopicName;
    ros::Subscriber droneSpeedsRefSub;
    void droneSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg);

    std::string droneTrajectoryAbsRefTopicName;
    ros::Subscriber droneTrajectoryAbsRefSub;  // Trajectory specified in world coordinates
    void droneTrajectoryAbsRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);

    std::string droneTrajectoryRelRefTopicName;
    ros::Subscriber droneTrajectoryRelRefSub;  // Trajectory specified relative to current drone pose
    void droneTrajectoryRelRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg);
    bool stay_in_position;

    std::string droneYawRefCommandTopicName;
    ros::Subscriber droneYawRefCommandSub;
    void droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr& msg);
    droneMsgsROS::droneYawRefCommand current_yaw_reference;

    // [Publishers] Controller references, meaning of useful values depend on the current control mode
    // useful values: x, y, z, yaw
    std::string drone_position_reference_topic_name;
    ros::Publisher drone_position_reference_publisher;
    droneMsgsROS::dronePositionRefCommand current_drone_position_reference;
    droneMsgsROS::dronePose   current_drone_pose_reference;

    ros::Publisher drone_yaw_reference_publisher;

    // useful values: vxfi, vyfi, vzfi (dyawfi is unused, undebugged)
    std::string drone_speed_reference_topic_name;
    ros::Publisher drone_speed_reference_publisher;
    droneMsgsROS::droneSpeeds current_drone_speed_reference;

    // useful values: current trajectory: initial_checkpoint,isPeridioc, checkpoints[]
    std::string drone_trajectory_reference_topic_name;
    ros::Publisher drone_trajectory_reference_publisher;
    droneMsgsROS::dronePositionTrajectoryRefCommand current_drone_trajectory_command;

    std::vector<SimpleTrajectoryWaypoint> waypoints;
    droneMsgsROS::dronePositionTrajectoryRefCommand trajectory_command;
    droneMsgsROS::dronePositionTrajectoryRefCommand actual_trajectory;
    bool traj_end;
    droneMsgsROS::dronePositionTrajectoryRefCommand rel_trajectory_command;



    void publishControllerReferences();
    void publishStopReferences();


    // [Subscribers] State estimation: controller feedback inputs
private:
    std::string droneEstimatedPoseTopicName;
    ros::Subscriber droneEstimatedPoseSubs;
    void droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr& msg);
    droneMsgsROS::dronePose last_estimatedPose;

    std::string droneEstimatedSpeedsTopicName;
    ros::Subscriber droneEstimatedSpeedsSubs;
    void droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr& msg);
    droneMsgsROS::droneSpeeds last_estimatedSpeed;

    int num, i, j;
    bool equal;
    // Publishers
private:
    std::string controlModeTopicName;
    ros::Publisher controlModePub;
    void publishControlMode();

    std::string drone_command_pitch_roll_topic_name;
    ros::Publisher drone_command_pitch_roll_publisher;
    droneMsgsROS::dronePitchRollCmd drone_command_pitch_roll_msg;

    std::string drone_command_dyaw_topic_name;
    ros::Publisher drone_command_dyaw_publisher;
    droneMsgsROS::droneDYawCmd      drone_command_dyaw_msg;

    std::string drone_command_daltitude_topic_name;
    ros::Publisher drone_command_daltitude_publisher;
    droneMsgsROS::droneDAltitudeCmd drone_command_daltitude_msg;

    Controller_MidLevel_controlMode::controlMode control_mode, init_control_mode;// Active and initial control modes
    inline Controller_MidLevel_controlMode::controlMode getControlMode() { return control_mode; }
    inline Controller_MidLevel_controlMode::controlMode getInitControlMode() { return init_control_mode; }

    int publishDroneNavCommand(void);
    void setNavCommand(float pitch, float roll, float dyaw, float dz, double time=-1.0);
    void setNavCommandToZero();
    void getOutput(float *pitch_val, float *roll_val, float *dyaw_val, float *dz_val);

    void setTrajectoryControl();
    void setPositionControl();
    void setSpeedControl();
    void setPosCmdControl();



    // Service servers
private:
    std::string setControlModeServiceName;
    ros::ServiceServer setControlModeServerSrv;
    bool setControlModeServCall(droneMsgsROS::setControlMode::Request& request, droneMsgsROS::setControlMode::Response& response);

    bool setControlMode(Controller_MidLevel_controlMode::controlMode mode);
};

#endif // flight_motion_controller_process
