#include "flight_motion_controller_process.h"

FlightMotionControllerProcess::FlightMotionControllerProcess() :
DroneModule(droneModule::active, 30.0) ,
altitude_control(),
yaw_control(),
position_control(),
speed_control(),
trajectory_control(),
poscmd_control()
{
   stay_in_position = false;
}

FlightMotionControllerProcess::~FlightMotionControllerProcess()
{}

bool FlightMotionControllerProcess::readConfigs(std::string configFile)
{
  try
  {
      XMLFileReader my_xml_reader(configFile);
      set_moduleRate(my_xml_reader.readDoubleValue("flight_motion_controller_config:module_frequency"));
      std::string init_control_mode_str;
      init_control_mode_str = my_xml_reader.readStringValue( "flight_motion_controller_config:init_control_mode" );
      if ( init_control_mode_str.compare("speed") == 0 ) {
          init_control_mode = Controller_MidLevel_controlMode::SPEED_CONTROL;
      } else {
          if ( init_control_mode_str.compare("position") == 0 ) {
              init_control_mode = Controller_MidLevel_controlMode::POSITION_CONTROL;
          } else {          // "trajectory"
              init_control_mode = Controller_MidLevel_controlMode::SPEED_CONTROL;
              throw std::runtime_error("Controller_MidLevel_SpeedLoop::Controller_MidLevel_SpeedLoop, initial control_mode cannot be TRAJECTORY_CONTROL");
              return false;
          }
      }
  }
  catch ( cvg_XMLFileReader_exception &e)
  {
      throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
  }


}


void FlightMotionControllerProcess::init()
{
    readConfigs(stackPath+"configs/drone"+cvg_int_to_string(idDrone)+"/"+config_file);
    altitude_control.setUp();
    yaw_control.setUp();
    position_control.setUp();
    poscmd_control.setUp();
    speed_control.setUp();
    trajectory_control.setUp();
    actual_trajectory.droneTrajectory.clear();
    actual_trajectory.initial_checkpoint = 0;
    actual_trajectory.is_periodic = 0;

    control_mode = init_control_mode;

    setControlMode(control_mode);


}

void FlightMotionControllerProcess::close()
{ /* DroneModule::~DroneModule gets called automatically */ }

bool FlightMotionControllerProcess::resetValues()
{
    control_mode = init_control_mode;

    setControlMode(control_mode);

    stay_in_position = false;
    altitude_control.stop();
    yaw_control.stop();
    position_control.stop();
    poscmd_control.stop();
    speed_control.stop();
    trajectory_control.stop();
    position_control.setReference(last_estimatedPose.x, last_estimatedPose.y);
    poscmd_control.setReference(last_estimatedPose.x, last_estimatedPose.y);
    speed_control.setReference(0.0,0.0);
    yaw_control.setReference(last_estimatedPose.yaw,last_estimatedPose.yaw);
    altitude_control.setReference(last_estimatedPose.z);

    current_drone_position_reference.x = last_estimatedPose.x;
    current_drone_position_reference.y = last_estimatedPose.y;
    current_drone_position_reference.z = last_estimatedPose.z;
    current_drone_pose_reference.x = last_estimatedPose.x;
    current_drone_pose_reference.y = last_estimatedPose.y;
    current_drone_pose_reference.z = last_estimatedPose.z;
    current_drone_pose_reference.yaw = last_estimatedPose.yaw;
    current_drone_pose_reference.pitch = 0.0;
    current_drone_pose_reference.roll = 0.0;
    current_drone_speed_reference.dx = 0.0;
    current_drone_speed_reference.dy = 0.0;
    publishControllerReferences();
    setNavCommandToZero();

    altitude_control.start();
    yaw_control.start();
    position_control.start();
    poscmd_control.start();
    speed_control.start();
    trajectory_control.start();

    return true;
}

bool FlightMotionControllerProcess::startVal()
{
    if ( !isStarted() )
    {
        resetValues();
        publishStopReferences();
        setNavCommandToZero();
    }
    return DroneModule::startVal();
}

bool FlightMotionControllerProcess::stopVal()
{
    setNavCommandToZero();
    return DroneModule::stopVal();
}

bool FlightMotionControllerProcess::run()
{
    DroneModule::run();
    publishControlMode();

    if (!moduleStarted)
    {
        return false;
    }

    float pitch, roll, dyaw, dz;
    publishControllerReferences();
    getOutput( &pitch, &roll, &dyaw, &dz);
    setNavCommand( pitch, roll, dyaw, dz);
    return true;
}

void FlightMotionControllerProcess::readParameters()
{
    // Configs
    //
    ros::param::get("~config_file", config_file);
    if ( config_file.length() == 0)
    {
        config_file="flight_motion_controller_config.xml";
    }
    std::cout<<"config_file="<<config_file<<std::endl;


    // Topics
    //
    ros::param::get("~drone_position_ref_topic_name", dronePositionRefTopicName);
    if ( dronePositionRefTopicName.length() == 0)
    {
        dronePositionRefTopicName="dronePositionRefs";
    }
    std::cout<<"drone_position_ref_topic_name="<<dronePositionRefTopicName<<std::endl;
    //
    ros::param::get("~drone_speeds_ref_topic_name", droneSpeedsRefTopicName);
    if ( droneSpeedsRefTopicName.length() == 0)
    {
        droneSpeedsRefTopicName="droneSpeedsRefs";
    }
    std::cout<<"drone_speeds_ref_topic_name="<<droneSpeedsRefTopicName<<std::endl;
    //
    ros::param::get("~drone_trajectory_abs_ref_topic_name", droneTrajectoryAbsRefTopicName);
    if ( droneTrajectoryAbsRefTopicName.length() == 0)
    {
        droneTrajectoryAbsRefTopicName="droneTrajectoryAbsRefCommand";
    }
    std::cout<<"drone_trajectory_abs_ref_topic_name="<<droneTrajectoryAbsRefTopicName<<std::endl;
    //
    ros::param::get("~drone_trajectory_rel_ref_topic_name", droneTrajectoryRelRefTopicName);
    if ( droneTrajectoryRelRefTopicName.length() == 0)
    {
        droneTrajectoryRelRefTopicName="droneTrajectoryRefCommand";
    }
    std::cout<<"drone_trajectory_rel_ref_topic_name="<<droneTrajectoryRelRefTopicName<<std::endl;
    //
    ros::param::get("~drone_yaw_ref_command_topic_name", droneYawRefCommandTopicName);
    if ( droneYawRefCommandTopicName.length() == 0)
    {
        droneYawRefCommandTopicName="droneControllerYawRefCommand";
    }
    std::cout<<"drone_yaw_ref_command_topic_name="<<droneYawRefCommandTopicName<<std::endl;
    //
    ros::param::get("~drone_position_reference_topic_name", drone_position_reference_topic_name);
    if ( drone_position_reference_topic_name.length() == 0)
    {
        drone_position_reference_topic_name="trajectoryControllerPositionReferencesRebroadcast";
    }
    std::cout<<"drone_position_reference_topic_name="<<drone_position_reference_topic_name<<std::endl;
    //
    ros::param::get("~drone_speed_reference_topic_name", drone_speed_reference_topic_name);
    if ( drone_speed_reference_topic_name.length() == 0)
    {
        drone_speed_reference_topic_name="trajectoryControllerSpeedReferencesRebroadcast";
    }
    std::cout<<"drone_speed_reference_topic_name="<<drone_speed_reference_topic_name<<std::endl;
    //
    ros::param::get("~drone_trajectory_reference_topic_name", drone_trajectory_reference_topic_name);
    if ( drone_trajectory_reference_topic_name.length() == 0)
    {
        drone_trajectory_reference_topic_name="trajectoryControllerTrajectoryReferencesRebroadcast";
    }
    std::cout<<"drone_trajectory_reference_topic_name="<<drone_trajectory_reference_topic_name<<std::endl;
    //
    ros::param::get("~drone_estimated_pose_topic_name", droneEstimatedPoseTopicName);
    if ( droneEstimatedPoseTopicName.length() == 0)
    {
        droneEstimatedPoseTopicName="EstimatedPose_droneGMR_wrt_GFF";
    }
    std::cout<<"drone_estimated_pose_topic_name="<<droneEstimatedPoseTopicName<<std::endl;
    //
    ros::param::get("~drone_estimated_speeds_topic_name", droneEstimatedSpeedsTopicName);
    if ( droneEstimatedSpeedsTopicName.length() == 0)
    {
        droneEstimatedSpeedsTopicName="EstimatedSpeed_droneGMR_wrt_GFF";
    }
    std::cout<<"drone_estimated_speeds_topic_name="<<droneEstimatedSpeedsTopicName<<std::endl;
    //
    ros::param::get("~control_mode_topic_name", controlModeTopicName);
    if ( controlModeTopicName.length() == 0)
    {
        controlModeTopicName="controlMode";
    }
    std::cout<<"control_mode_topic_name="<<controlModeTopicName<<std::endl;
    //
    ros::param::get("~drone_command_pitch_roll_topic_name", drone_command_pitch_roll_topic_name);
    if ( drone_command_pitch_roll_topic_name.length() == 0)
    {
        drone_command_pitch_roll_topic_name="command/pitch_roll";
    }
    std::cout<<"drone_command_pitch_roll_topic_name="<<drone_command_pitch_roll_topic_name<<std::endl;
    //
    ros::param::get("~drone_command_dyaw_topic_name", drone_command_dyaw_topic_name);
    if ( drone_command_dyaw_topic_name.length() == 0)
    {
        drone_command_dyaw_topic_name="command/dYaw";
    }
    std::cout<<"drone_command_dyaw_topic_name="<<drone_command_dyaw_topic_name<<std::endl;
    //
    ros::param::get("~drone_command_daltitude_topic_name", drone_command_daltitude_topic_name);
    if ( drone_command_daltitude_topic_name.length() == 0)
    {
        drone_command_daltitude_topic_name="command/dAltitude";
    }
    std::cout<<"drone_command_daltitude_topic_name="<<drone_command_daltitude_topic_name<<std::endl;


    // Service
    //
    ros::param::get("~set_control_mode_service_name", setControlModeServiceName);
    if ( setControlModeServiceName.length() == 0)
    {
        setControlModeServiceName="setControlMode";
    }
    std::cout<<"set_control_mode_service_name="<<setControlModeServiceName<<std::endl;

    return;
}

void FlightMotionControllerProcess::open(ros::NodeHandle & nIn)
{
    //Node
    DroneModule::open(nIn);

    // Parameters
    readParameters();

    // Init
    init();

    //// Topics ///
    // Subscribers
    dronePositionRefSub     = n.subscribe(dronePositionRefTopicName, 1, &FlightMotionControllerProcess::dronePositionRefsSubCallback, this);
    droneSpeedsRefSub       = n.subscribe(droneSpeedsRefTopicName,    1, &FlightMotionControllerProcess::droneSpeedsRefsSubCallback, this);
    droneTrajectoryAbsRefSub= n.subscribe(droneTrajectoryAbsRefTopicName,   1, &FlightMotionControllerProcess::droneTrajectoryAbsRefCommandCallback, this);
    droneTrajectoryRelRefSub= n.subscribe(droneTrajectoryRelRefTopicName,   1, &FlightMotionControllerProcess::droneTrajectoryRelRefCommandCallback, this);
    droneYawRefCommandSub   = n.subscribe(droneYawRefCommandTopicName, 1, &FlightMotionControllerProcess::droneYawRefCommandCallback, this);
    droneEstimatedPoseSubs   = n.subscribe(droneEstimatedPoseTopicName,      1, &FlightMotionControllerProcess::droneEstimatedPoseCallback, this);
    droneEstimatedSpeedsSubs = n.subscribe(droneEstimatedSpeedsTopicName,    1, &FlightMotionControllerProcess::droneEstimatedSpeedsCallback, this);

    // Publishers
    controlModePub      = n.advertise<droneMsgsROS::droneTrajectoryControllerControlMode>(ros::this_node::getName()+"/"+controlModeTopicName, 1);
    drone_command_pitch_roll_publisher = n.advertise<droneMsgsROS::dronePitchRollCmd>(drone_command_pitch_roll_topic_name, 1);
    drone_command_dyaw_publisher      = n.advertise<droneMsgsROS::droneDYawCmd>(drone_command_dyaw_topic_name, 1);
    drone_command_daltitude_publisher = n.advertise<droneMsgsROS::droneDAltitudeCmd>(drone_command_daltitude_topic_name, 1);

    drone_position_reference_publisher   = n.advertise<droneMsgsROS::dronePose>(drone_position_reference_topic_name, 1);
    drone_yaw_reference_publisher = n.advertise<droneMsgsROS::droneYawRefCommand>(droneYawRefCommandTopicName,1);
    drone_speed_reference_publisher      = n.advertise<droneMsgsROS::droneSpeeds>(drone_speed_reference_topic_name, 1);
    drone_trajectory_reference_publisher = n.advertise<droneMsgsROS::dronePositionTrajectoryRefCommand>(drone_trajectory_reference_topic_name, 1);

    // Service servers
    setControlModeServerSrv = n.advertiseService(ros::this_node::getName()+"/"+setControlModeServiceName,&FlightMotionControllerProcess::setControlModeServCall,this);

    //Flag of module opened
    droneModuleOpened=true;

    //End
    return;
}


void FlightMotionControllerProcess::dronePositionRefsSubCallback(const droneMsgsROS::dronePositionRefCommandStamped::ConstPtr &msg) {      //! what for
    current_drone_position_reference = (msg->position_command);
}

void FlightMotionControllerProcess::droneSpeedsRefsSubCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg) {
    current_drone_speed_reference  = (*msg);
}

void FlightMotionControllerProcess::droneTrajectoryAbsRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr& msg) {
    trajectory_command = (*msg);
    int n = trajectory_command.droneTrajectory.size();

    if(n==0 && (!stay_in_position) ) {
        droneMsgsROS::dronePositionRefCommand actual_position;
        actual_position.x = +last_estimatedPose.x;
        actual_position.y = +last_estimatedPose.y;
        actual_position.z = +last_estimatedPose.z;
        trajectory_command.droneTrajectory.push_back(actual_position);
        trajectory_command.is_periodic        = false;
        trajectory_command.initial_checkpoint = 0;
        n = 1;
        stay_in_position = true;
    } else {
        if ( n>0 ) {
            stay_in_position = false;
        } else {
            return;
        }
    }
    setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);

    return;
}

void FlightMotionControllerProcess::droneTrajectoryRelRefCommandCallback(const droneMsgsROS::dronePositionTrajectoryRefCommand::ConstPtr &msg)
{

    rel_trajectory_command = (*msg);
    trajectory_command = (*msg);

    int n = rel_trajectory_command.droneTrajectory.size();
    if(n==0 && (!stay_in_position) ) {
        droneMsgsROS::dronePositionRefCommand actual_position;
        actual_position.x = 0.0;
        actual_position.y = 0.0;
        actual_position.z = 0.0;
        rel_trajectory_command.droneTrajectory.push_back(actual_position);
        rel_trajectory_command.is_periodic        = false;
        rel_trajectory_command.initial_checkpoint = 0;
        n = 1;
        stay_in_position = true;
    } else {
        if ( n>0 ) {
            stay_in_position = false;
        } else {
            return;
        }
    }

    trajectory_command.droneTrajectory.clear();
    std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it_mod = (rel_trajectory_command.droneTrajectory).begin();
    for (std::vector<droneMsgsROS::dronePositionRefCommand>::const_iterator it = (rel_trajectory_command.droneTrajectory).begin();
         it != (rel_trajectory_command.droneTrajectory).end();
         ++it) {

        droneMsgsROS::dronePositionRefCommand next_waypoint;
        next_waypoint.x = it_mod->x;
        next_waypoint.y = it_mod->y;
        next_waypoint.z = it_mod->z;
        next_waypoint.x += last_estimatedPose.x;
        next_waypoint.y += last_estimatedPose.y;
        next_waypoint.z += last_estimatedPose.z;

        trajectory_command.droneTrajectory.push_back(next_waypoint);
        std::cout<<next_waypoint<<std::endl;
    }

    setControlMode(Controller_MidLevel_controlMode::TRAJECTORY_CONTROL);

    return;
}

void FlightMotionControllerProcess::droneYawRefCommandCallback(const droneMsgsROS::droneYawRefCommand::ConstPtr &msg) {
    current_yaw_reference = (*msg) ;
}


void FlightMotionControllerProcess::droneEstimatedPoseCallback(const droneMsgsROS::dronePose::ConstPtr &msg) {
    last_estimatedPose = (*msg);
}

void FlightMotionControllerProcess::droneEstimatedSpeedsCallback(const droneMsgsROS::droneSpeeds::ConstPtr &msg) {
    last_estimatedSpeed = (*msg);
    return;
}


void FlightMotionControllerProcess::publishControllerReferences() {
    current_drone_pose_reference.x = current_drone_position_reference.x;
    current_drone_pose_reference.y = current_drone_position_reference.y;
    current_drone_pose_reference.z = current_drone_position_reference.z;
    current_drone_pose_reference.yaw = current_yaw_reference.yaw;
    current_drone_pose_reference.pitch = 0.0;
    current_drone_pose_reference.roll = 0.0;

    current_drone_speed_reference.dpitch = 0.0;
    current_drone_speed_reference.droll  = 0.0;


    if (droneModuleOpened) {
        drone_position_reference_publisher.publish(current_drone_pose_reference);
        drone_yaw_reference_publisher.publish(current_yaw_reference);
        drone_speed_reference_publisher.publish(current_drone_speed_reference);
        drone_trajectory_reference_publisher.publish(trajectory_command);
    }
}
void FlightMotionControllerProcess::publishStopReferences() {
    // get current_drone_position_reference
    current_drone_pose_reference.x = last_estimatedPose.x;
    current_drone_pose_reference.y = last_estimatedPose.y;
    current_drone_pose_reference.z = last_estimatedPose.z;
    current_drone_pose_reference.yaw = last_estimatedPose.yaw;
    current_drone_pose_reference.pitch = 0.0;
    current_drone_pose_reference.roll = 0.0;

    current_yaw_reference.yaw = last_estimatedPose.yaw;

    current_drone_speed_reference.dpitch = 0.0;
    current_drone_speed_reference.droll  = 0.0;

    trajectory_command.initial_checkpoint = 0;
    trajectory_command.is_periodic = false;
    trajectory_command.droneTrajectory.clear();
    Controller_MidLevel_controlMode::controlMode last_control;
    last_control = control_mode;
    setControlMode(Controller_MidLevel_controlMode::controlMode::POSITION_CONTROL);
    if (droneModuleOpened) {
        drone_position_reference_publisher.publish(current_drone_pose_reference);
        drone_yaw_reference_publisher.publish(current_yaw_reference);
        drone_speed_reference_publisher.publish(current_drone_speed_reference);
        drone_trajectory_reference_publisher.publish(trajectory_command);
    }

}

void FlightMotionControllerProcess::publishControlMode() {
    droneMsgsROS::droneTrajectoryControllerControlMode controlModeMsg;
    controlModeMsg.command = getControlMode();

    if (droneModuleOpened)
        controlModePub.publish(controlModeMsg);
}

int FlightMotionControllerProcess::publishDroneNavCommand() {
    if(droneModuleOpened==false)
        return 0;

    drone_command_pitch_roll_publisher.publish(drone_command_pitch_roll_msg);
    drone_command_dyaw_publisher.publish(drone_command_dyaw_msg);
    drone_command_daltitude_publisher.publish(drone_command_daltitude_msg);
    return 1;
}

void FlightMotionControllerProcess::setNavCommand(float pitch, float roll, float dyaw, float dz, double time) {
    drone_command_pitch_roll_msg.header.stamp = ros::Time::now();
    drone_command_dyaw_msg.header.stamp       = ros::Time::now();
    drone_command_daltitude_msg.header.stamp  = ros::Time::now();

    drone_command_pitch_roll_msg.pitchCmd    = pitch;
    drone_command_pitch_roll_msg.rollCmd     = roll;
    drone_command_dyaw_msg.dYawCmd           = dyaw;
    drone_command_daltitude_msg.dAltitudeCmd = dz;

    publishDroneNavCommand();
    return;
}

void FlightMotionControllerProcess::setNavCommandToZero(void) {
    setNavCommand(0.0,0.0,0.0,0.0,-1.0);
    return;
}
bool FlightMotionControllerProcess::setControlMode(Controller_MidLevel_controlMode::controlMode mode){
    switch (mode) {
    case Controller_MidLevel_controlMode::TRAJECTORY_CONTROL:
        setTrajectoryControl();
        break;
    case Controller_MidLevel_controlMode::POSITION_CONTROL:
        setPositionControl();
        break;
    case Controller_MidLevel_controlMode::POSITION_TO_COMMAND_CONTROL:
        setPosCmdControl();
        break;
    case Controller_MidLevel_controlMode::SPEED_CONTROL:
        setSpeedControl();
        break;
    case Controller_MidLevel_controlMode::UNKNOWN_CONTROL_MODE:
    default:
        return false;
        break;
    }
    control_mode = mode;

    return true;
}

bool FlightMotionControllerProcess::setControlModeServCall(droneMsgsROS::setControlMode::Request& request, droneMsgsROS::setControlMode::Response& response) {

    Controller_MidLevel_controlMode::controlMode new_control_mode;

    switch (request.controlMode.command) {
    case Controller_MidLevel_controlMode::TRAJECTORY_CONTROL:
        new_control_mode = Controller_MidLevel_controlMode::TRAJECTORY_CONTROL;
        break;
    case Controller_MidLevel_controlMode::POSITION_CONTROL:
        new_control_mode = Controller_MidLevel_controlMode::POSITION_CONTROL;
        break;
    case Controller_MidLevel_controlMode::SPEED_CONTROL:
        new_control_mode = Controller_MidLevel_controlMode::SPEED_CONTROL;
        break;
    case Controller_MidLevel_controlMode::PBVS_TRACKER_IS_REFERENCE:
        new_control_mode = Controller_MidLevel_controlMode::PBVS_TRACKER_IS_REFERENCE;
        break;
    case Controller_MidLevel_controlMode::PBVS_TRACKER_IS_FEEDBACK:
        new_control_mode = Controller_MidLevel_controlMode::PBVS_TRACKER_IS_FEEDBACK;
        break;
    case Controller_MidLevel_controlMode::PBVS_TRACKER_IS_FEEDBACK_TRACKER_LOST:
        new_control_mode = Controller_MidLevel_controlMode::PBVS_TRACKER_IS_FEEDBACK_TRACKER_LOST;
        break;
    case Controller_MidLevel_controlMode::POSITION_TO_COMMAND_CONTROL:
        new_control_mode = Controller_MidLevel_controlMode::POSITION_TO_COMMAND_CONTROL;
        break;
    case Controller_MidLevel_controlMode::UNKNOWN_CONTROL_MODE:
    default:
        new_control_mode = Controller_MidLevel_controlMode::UNKNOWN_CONTROL_MODE;
        break;
    }
    response.ack = setControlMode(new_control_mode);
    return response.ack;
}

void FlightMotionControllerProcess::getOutput(float *pitch_val, float *roll_val, float *dyaw_val, float *dz_val){
    float pitch, roll, dyaw, dx, dy, dz;
    switch (control_mode)
    {
    case Controller_MidLevel_controlMode::TRAJECTORY_CONTROL: //chek if trajectory is the same (ifnot change to new trajectory or maintain/restart if it's the same)
        num = actual_trajectory.droneTrajectory.size();
        j = trajectory_command.droneTrajectory.size();
        equal = true;
        if (num == j){
            for (i=0; i<num; i++){
                if (actual_trajectory.droneTrajectory[i].x != trajectory_command.droneTrajectory[i].x)
                    equal = false;
                if (actual_trajectory.droneTrajectory[i].y != trajectory_command.droneTrajectory[i].y)
                    equal = false;
                if (actual_trajectory.droneTrajectory[i].z != trajectory_command.droneTrajectory[i].z)
                    equal = false;
            }
        }else
            equal = false;

        if ((equal)&&(actual_trajectory.initial_checkpoint == trajectory_command.initial_checkpoint)
                &&(actual_trajectory.is_periodic == trajectory_command.is_periodic)){
            if (traj_end == true){
                publishStopReferences();
                setNavCommandToZero();

                pitch = 0.0;
                roll = 0.0;
                dyaw = 0.0;
                dz = 0.0;
            }else{
//                trajectory_control.setFeedback(last_estimatedPose, last_estimatedSpeed);
//                trajectory_control.getOutput(&pitch, &roll, &dz, &yaw, &traj_end);

//                yaw_control.setFeedback(last_estimatedPose.yaw);
//                yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
//                yaw_control.getOutput(&dyaw);
//            }
//        }else{
//            actual_trajectory = trajectory_command;
//            traj_end = false;
//            trajectory_control.setReference(actual_trajectory);
//            trajectory_control.setFeedback(last_estimatedPose, last_estimatedSpeed);
//            trajectory_control.getOutput(&pitch, &roll, &dz, &yaw, &traj_end);

//            yaw_control.setFeedback(last_estimatedPose.yaw);
//            yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
//            yaw_control.getOutput(&dyaw);
//        }
                trajectory_control.setFeedback(last_estimatedPose, last_estimatedSpeed);
                trajectory_control.getOutput(&dx, &dy, &dz, &traj_end);

                speed_control.setFeedback(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
                speed_control.setReference(dx, dy);
                speed_control.getOutput( &pitch, &roll, last_estimatedPose.yaw);

                yaw_control.setFeedback(last_estimatedPose.yaw);
                yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
                yaw_control.getOutput(&dyaw);
            }
        }else{
            actual_trajectory = trajectory_command;
            traj_end = false;
            trajectory_control.setReference(actual_trajectory);
            trajectory_control.setFeedback(last_estimatedPose, last_estimatedSpeed);
            trajectory_control.getOutput(&dx, &dy, &dz, &traj_end);

            speed_control.setFeedback(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
            speed_control.setReference(dx, dy);
            speed_control.getOutput( &pitch, &roll, last_estimatedPose.yaw);

            yaw_control.setFeedback(last_estimatedPose.yaw);
            yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
            yaw_control.getOutput(&dyaw);
        }


        break;
    case Controller_MidLevel_controlMode::SPEED_CONTROL:    //set speeds refs, getoutput from speed; yaw and height in parallel
        speed_control.setFeedback(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
        speed_control.setReference(current_drone_speed_reference.dx, current_drone_speed_reference.dy);
        speed_control.getOutput( &pitch, &roll, last_estimatedPose.yaw);

        yaw_control.setFeedback(last_estimatedPose.yaw);
        yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
        yaw_control.getOutput(&dyaw);

        altitude_control.setFeedback(last_estimatedPose.z);
        altitude_control.setReference(current_drone_position_reference.z);
        altitude_control.getOutput(&dz);

        break;
    case Controller_MidLevel_controlMode::POSITION_CONTROL: //set pos ref, set getoutput from pos as speeds refs, getoutput from speed; yaw and height in parallel;

        float dx, dy;
        position_control.setFeedback(last_estimatedPose.x, last_estimatedPose.y);
        position_control.setReference(current_drone_position_reference.x, current_drone_position_reference.y);
        position_control.getOutput( &dx, &dy);

        speed_control.setFeedback(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
        speed_control.setReference(dx, dy);
        speed_control.getOutput( &pitch, &roll, last_estimatedPose.yaw);

        yaw_control.setFeedback(last_estimatedPose.yaw);
        yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
        yaw_control.getOutput(&dyaw);

        altitude_control.setFeedback(last_estimatedPose.z);
        altitude_control.setReference(current_drone_position_reference.z);
        altitude_control.getOutput(&dz);
        break;
    case Controller_MidLevel_controlMode::POSITION_TO_COMMAND_CONTROL: //pos, yaw and height in parallel;

        poscmd_control.setFeedback(last_estimatedPose.x, last_estimatedPose.y);
        poscmd_control.setReference(current_drone_position_reference.x, current_drone_position_reference.y);
        poscmd_control.getOutput( &pitch, &roll, last_estimatedPose.yaw);

        yaw_control.setFeedback(last_estimatedPose.yaw);
        yaw_control.setReference(current_yaw_reference.yaw, last_estimatedPose.yaw);
        yaw_control.getOutput(&dyaw);

        altitude_control.setFeedback(last_estimatedPose.z);
        altitude_control.setReference(current_drone_position_reference.z);
        altitude_control.getOutput(&dz);
        break;
    case Controller_MidLevel_controlMode::UNKNOWN_CONTROL_MODE:
        setNavCommandToZero();

        pitch = 0.0;
        roll = 0.0;
        dyaw = 0.0;
        dz = 0.0;

    default:
        break;
    }

    *pitch_val = pitch;
    *roll_val  = roll;
    *dyaw_val  = dyaw;
    *dz_val    = dz;
}

//void FlightMotionControllerProcess::setTrajectoryControl(){

//    if (Controller_MidLevel_controlMode::TRAJECTORY_CONTROL != control_mode){
//        actual_trajectory.droneTrajectory.clear();
//        trajectory_control.stop();
//        yaw_control.stop();
//        trajectory_control.start();
//        yaw_control.start();
//        yaw_control.setReference(last_estimatedPose.yaw, last_estimatedPose.yaw);
//        altitude_control.stop();
//        position_control.stop();
//        speed_control.stop();
//    }
//}

void FlightMotionControllerProcess::setTrajectoryControl(){

    if (Controller_MidLevel_controlMode::TRAJECTORY_CONTROL != control_mode){
        actual_trajectory.droneTrajectory.clear();
        trajectory_control.stop();
        speed_control.stop();
        yaw_control.stop();
        trajectory_control.start();
        speed_control.start();
        speed_control.setReference(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
        yaw_control.start();
        yaw_control.setReference(last_estimatedPose.yaw, last_estimatedPose.yaw);
        altitude_control.stop();
        position_control.stop();
        poscmd_control.stop();
    }
}

void FlightMotionControllerProcess::setPositionControl(){

    if(Controller_MidLevel_controlMode::POSITION_CONTROL != control_mode){
        if (control_mode == Controller_MidLevel_controlMode::SPEED_CONTROL){
            position_control.stop();
            position_control.start();
            position_control.setReference(last_estimatedPose.x, last_estimatedPose.y);
            trajectory_control.stop(); // it should be already stoped, but it wont affect
        }else{ //trajectory or unknown
            position_control.stop();
            poscmd_control.stop();
            speed_control.stop();
            altitude_control.stop();
            yaw_control.stop();
            trajectory_control.stop();
            position_control.start();
            position_control.setReference(last_estimatedPose.x, last_estimatedPose.y);
            speed_control.start();
            speed_control.setReference(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
            altitude_control.start();
            altitude_control.setReference(last_estimatedPose.z);
            yaw_control.start();
            yaw_control.setReference(last_estimatedPose.yaw, last_estimatedPose.yaw);
        }
    }
}

void FlightMotionControllerProcess::setPosCmdControl(){

    if(Controller_MidLevel_controlMode::POSITION_CONTROL != control_mode){
        if (control_mode == Controller_MidLevel_controlMode::POSITION_CONTROL){
            position_control.stop();
            speed_control.stop();
            trajectory_control.stop(); // it should be already stoped, but it wont affect
            poscmd_control.start();
            poscmd_control.setReference(last_estimatedPose.x,last_estimatedPose.y);
        }else if (control_mode == Controller_MidLevel_controlMode::SPEED_CONTROL){
            speed_control.stop();
            trajectory_control.stop(); // it should be already stoped, but it wont affect
            position_control.stop(); // it should be already stoped, but it wont affect
            poscmd_control.start();
            poscmd_control.setReference(last_estimatedPose.x,last_estimatedPose.y);
            } else {
            //trajectory or unknown
            position_control.stop();
            poscmd_control.stop();
            speed_control.stop();
            altitude_control.stop();
            yaw_control.stop();
            trajectory_control.stop();
            poscmd_control.start();
            poscmd_control.setReference(last_estimatedPose.x, last_estimatedPose.y);
            altitude_control.start();
            altitude_control.setReference(last_estimatedPose.z);
            yaw_control.start();
            yaw_control.setReference(last_estimatedPose.yaw, last_estimatedPose.yaw);
        }
    }
}


void FlightMotionControllerProcess::setSpeedControl(){

    if(Controller_MidLevel_controlMode::SPEED_CONTROL != control_mode){
        if (control_mode == Controller_MidLevel_controlMode::POSITION_CONTROL){
            position_control.stop();
            trajectory_control.stop(); // it should be already stoped, but it wont affect
        }else{ //trajectory or unknown
            speed_control.stop();
            altitude_control.stop();
            yaw_control.stop();
            trajectory_control.stop();
            position_control.stop();
            poscmd_control.stop();
            speed_control.start();
            speed_control.setReference(last_estimatedSpeed.dx, last_estimatedSpeed.dy);
            altitude_control.start();
            altitude_control.setReference(last_estimatedPose.z);
            yaw_control.start();
            yaw_control.setReference(last_estimatedPose.yaw, last_estimatedPose.yaw);

        }
    }
}
