//////////////////////////////////////////////////////
//  flight_motion_controller_process_main.cpp
//
//  Created on: Jul 5, 2018
//      Author: psantofimia
//
//  Last modification on: Dec 10, 2018
//      Author: psantofimia
//
//////////////////////////////////////////////////////

#include <iostream>
#include <math.h>

#include "ros/ros.h"
#include "flight_motion_controller_process.h"
#include "nodes_definition.h"

int main(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());
    ros::NodeHandle n;

    std::cout << "[ROSNODE] Starting "<<ros::this_node::getName() << std::endl;

    FlightMotionControllerProcess MyFlightMotionController;
    MyFlightMotionController.open(n);

    try
    {
        while(ros::ok())
        {
            //Read messages
            ros::spinOnce();

            //Run EKF State Estimator
            if(MyFlightMotionController.run())
            {
            }
            //Sleep
            MyFlightMotionController.sleep();
        }
    }
    catch (std::exception &ex)
    {
        std::cout << "[ROSNODE] Exception :" << ex.what() << std::endl;
    }
    return 0;
}
