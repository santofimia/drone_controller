# Flight motion controller

# Brief


# Services

## NEW services
- **flight_motion_controller/set_control_mode**([aerostack_msgs/ControlMode](null))
Changes trajectory's control mode.

## OLD services
- **drone_controller_process/setControlMode**([droneMsgsROS/setControlMode](https://bitbucket.org/joselusl/dronemsgsros/src/master/srv/setControlMode.srv))
Changes trajectory's control mode.


# Publish topics

## NEW topics

- **motion_reference/position** ([geometric_msgs/PointStamped](http://docs.ros.org/lunar/api/geometry_msgs/html/msg/PointStamped.html))  
Publishes position references for controllers.

- **motion_reference/speeds** ([geometric_msgs/TwistStamped](http://docs.ros.org/lunar/api/geometry_msgs/html/msg/TwistStamped.html))  
Publishes position references for controllers.

- **motion_reference/yaw** ([aerostack_msgs/Float32Stamped](null))  
Publishes yaw references for controllers.

- **motion_reference/trajectory** ([geometric_msgs/Polygon](http://docs.ros.org/lunar/api/geometry_msgs/html/msg/Polygon.html))  
Publishes trajectory references for controllers.

- **motion_reference/flight_motion_control_mode** ([aerostack_msgs/FlightMotionControlMode](null))  
Publishes controller's control mode.


## OLD Topics
- **command/dAltitude** ([droneMsgsROS/droneAltitudeCmd](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneAltitudeCmd.msg))  
Publishes altitude commands for controllers.

- **command/dYaw** ([droneMsgsROS/droneDYawCmd](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneDYawCmd.msg))  
Publishes yaw commands for controllers.

- **command/pitch_roll** ([droneMsgsROS/dronePitchRollCmd](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePitchRollCmd.msg))  
Publishes pitch and roll commands for controllers.

- **droneControllerYawRefCommand** ([droneMsgsROS/droneYawRefCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneYawRefCommand.msg))  
Publishes yaw reference data for controllers.

- **dronePositionRefs** ([droneMsgsROS/dronePositionRefCommandStamped](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePositionRefCommandStamped.msg))  
Publishes position references for controllers.

- **droneSpeedsRefs** ([droneMsgsROS/droneSpeedsStamped](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneSpeedsStamped.msg))  
Publishes speed references to controllers.

- **droneTrajectoryAbsRefCommand** ([droneMsgsROS/dronePositionTrajectoryRefCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePositionTrajectoryRefCommand.msg))  
Publishes a trajectory reference to controllers.

- **droneTrajectoryRefCommand** ([droneMsgsROS/dronePositionTrajectoryRefCommand](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePositionTrajectoryRefCommand.msg))  
Publishes a trajectory reference to controllers.

- **flight_motion_controller/controlMode** ([droneMsgsROS/droneTrajectoryControllerControlMode](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneTrajectoryControllerControlMode.msg))  
Publishes controller's control mode.

- **trajectoryControllerPositionReferencesRebroadcast** ([droneMsgsROS/dronePositionRefCommandStamped](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/dronePositionRefCommandStamped.msg))  
Broadcast a trajectory reference.

- **trajectoryControllerSpeedReferencesRebroadcast** ([droneMsgsROS/droneSpeedsStamped](https://bitbucket.org/joselusl/dronemsgsros/src/master/msg/droneSpeedsStamped.msg))  
Publishes speed references to controllers.

- **trajectoryControllerTrajectoryReferencesRebroadcast** ([droneMsgsROS/dronePositionTrajectoryRefCommand](https://bitbucket.org/visionaerialrobotics/aerostack_msgs/src/master/msg/ListOfProcesses.msg))  
Broadcast a trajectory reference.

# Tests
No tests created yet.

---
# Contributors
**Document writer:** Alberto Camporredondo
**Document reviewer:** Pablo Santofimia
**Code Maintainer:** Pablo Santofimia  
**Author:** Pablo Santofimia
